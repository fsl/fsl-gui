# FSL GUI

The FSL graphical user interface (GUI) is made up of multiple single purpose GUIs (bet, flirt, fast, etc.). The GUIs are developed together in this repository, but are published as separate conda packages installed via the FSL conda channel and FSL installer (to be done soon). 

## GUIs and screenshots table

| GUI | Description | Screenshot |
| --- | --- | --- |
| bet | Brain extraction tool | <img src="./packages/bet/screenshots/bet.png" alt="drawing" width="200" /> |
| fast | Tissue segmentation tool | <img src="./packages/fast/screenshots/fast.png" alt="drawing" width="200" /> |
| flirt | Linear registration tool | <img src="./packages/flirt/screenshots/flirt.png" alt="drawing" width="200" /> |
| applyxfm | Apply FLIRT transformation | <img src="./packages/applyxfm/screenshots/applyxfm.png" alt="drawing" width="200" /> |
| invertxfm | Invert FLIRT transformations | <img src="./packages/invertxfm/screenshots/invertxfm.png" alt="drawing" width="200" /> |
| concatxfm | Concatenate FLIRT transformations| <img src="./packages/concatxfm/screenshots/concatxfm.png" alt="drawing" width="200" /> |
| history | GUI command history | <img src="./packages/history/screenshots/history.png" alt="drawing" width="200" /> |

# Development server port mapping

During development, each individual GUI is served on a different port. The main Electron application knows about these ports and can load each GUI using this info when the --dev flag is provided. The port mapping is as follows:

| package (GUI) | port |
| --- | --- |
| bet | 5173 |
| fast | 5174 |
| flirt | 5175 |
| fslstats | 5176 |
| glm | 5177 |
| feat | 5178 |
| prepare_fieldmap | 5179 |
| fslmaths | 5180 |
| featquery | 5181 |
| melodic | 5182 |
| fdt | 5183 |
| history | 5184 |
| applyxfm | 5185 |
| invertxfm | 5186 |
| concatxfm | 5187 |

# Development

This project is managed using Lerna

## Development commands

| command | description |
| --- | --- |
| `npm install` | Installs all javascript package dependencies |
| `npm run bootstrap` | link packages together and optimise duplicate dependencies |
| `npm run dev` | Start all packages in development mode (hot reload is enabled for development) |





