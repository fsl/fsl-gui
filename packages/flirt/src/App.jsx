import { useState, useEffect } from 'react'
import './App.css'
import {fsljs} from 'fsljs'
import { 
  AppContainer, 
  Title, 
  FileChooser, 
  Row, 
  Column, 
  CommandPreview, 
  NiiVue,
  InputSlider,
  InputSelect
} from 'widgets'
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import Typography from '@mui/material/Typography';
import FormControlLabel from '@mui/material/FormControlLabel';

export default function App() {

  const [opts, setOpts] = useState(fsljs.defaultFlirtOpts)
  const [commandString, setCommandString] = useState('')
  const [running, setRunning] = useState(false)

  // watch for changes to the JSON string version of opts
  useEffect(() => {
    // create the command string for this tool (sends a message to the main process)
    async function createCommandString(){
      let command = 'flirt'
      let commandString = await fsljs.createCommandString({command, opts})
      setCommandString(commandString)
    }
    autoSetOutputFile()
    createCommandString()
  }, [JSON.stringify(opts)])

  const [optionsOpen, setOptionsOpen] = useState(false)

  async function onRunClick(){
    setRunning(true)
    let output = await fsljs.run({
      command: 'flirt',
      opts: opts
    })
    console.log('from main', output)
    setRunning(false)
  }

  function onOptionsClick(){
    setOptionsOpen(!optionsOpen)
  }

  const dofOptions = [
    {value: 3, label: 'Translation 3 DOF'},
    {value: 6, label: 'Rigid body 6 DOF'},
    {value: 7, label: 'Global rescale 7 DOF'},
    {value: 9, label: 'Traditional 9 DOF'},
    {value: 12, label: 'Affine 12 DOF'},
  ]

  const searchOptions = [
      {value: '-180 180', label: 'incorrectly oriented'},
      {value: '0 0', label: 'already virtually aligned (no search)'},
      {value: '-90 90', label: 'not aligned, but same orientation'},
  ]

  // mutualinfo,corratio,normcorr,normmi,leastsq,labeldiff,bbr
  const costOptions = [
      {value: 'corratio', label: 'corratio'},
      {value: 'normcorr', label: 'normcorr'},
      {value: 'normmi', label: 'normmi'},
      {value: 'leastsq', label: 'leastsq'},
      {value: 'mutualinfo', label: 'mutualinfo'},
  ]

  const interpOptions = [
      {value: 'trilinear', label: 'trilinear'},
      {value: 'nearestneighbour', label: 'nearestneighbour'},
      {value: 'sinc', label: 'sinc'},
      {value: 'spline', label: 'spline'},
  ]

  const sincOptions = [
      {value: 'rectangular', label: 'rectangular'},
      {value: 'hanning', label: 'hanning'},
      {value: 'blackman', label: 'blackman'},
  ]

  // onInputFileChange  
  async function onInputFileClick(){
    let results = await fsljs.openFileDialog()
    console.log(results)
    let inputFile = results.filePaths[0]
    // update the '-in' option
    setOpts({...opts, '-in': inputFile})
  }

  async function onInputWeightFileClick(){
    let results = await fsljs.openFileDialog()
    console.log(results)
    let inputFile = results.filePaths[0]
    // update the '-in' option
    setOpts({...opts, '-inweight': inputFile})
  }

  async function onRefWeightFileClick(){
    let results = await fsljs.openFileDialog()
    console.log(results)
    let inputFile = results.filePaths[0]
    // update the '-in' option
    setOpts({...opts, '-refweight': inputFile})
  }

  // onRefFileChange  
  async function onRefFileClick(){
    let results = await fsljs.openFileDialog()
    console.log(results)
    let inputFile = results.filePaths[0]
    // update the '-in' option
    setOpts({...opts, '-ref': inputFile})
  }

  async function onMNI1mmClick(){
    let refFile = await fsljs.onMNIReference(1)
    setOpts({...opts, '-ref': refFile})
  }

  async function onMNI2mmClick(){
    let refFile = await fsljs.onMNIReference(2)
    setOpts({...opts, '-ref': refFile})
  }

  function onDOFChange(value){
    setOpts({...opts, '-dof': Number(value)})
  }

  function autoSetOutputFile(){
    let inputFile = opts['-in']
    let refFile = opts['-ref']
    // do nothing if either inputFile or refFile are empty strings
    if (inputFile === '' || refFile === '' || inputFile === null || refFile === null) return
    let inputDirname = fsljs.dirname(inputFile)
    let inputBasename = fsljs.removeExtension(fsljs.basename(inputFile))
    let refBasename = fsljs.removeExtension(fsljs.basename(refFile))
    let outputFile = fsljs.join(inputDirname, `${inputBasename}_to_${refBasename}`)
    let outputMat = `${outputFile}.mat`
    setOpts({...opts, '-out': outputFile, '-omat': outputMat})
    console.log(outputFile)
  }

  function onSearchChange(value){
    setOpts({
      ...opts, 
      '-searchrx': value,
      '-searchry': value,
      '-searchrz': value,
    })
  }

  function onCostChange(value){
    setOpts({...opts, '-cost': value})
  }

  function onHistBinsChange(value){
    setOpts({...opts, '-bins': value})
  }

  function onInterpChange(value){
    setOpts({...opts, '-interp': value})
  }

  function onSincWindowChange(value){
    setOpts({...opts, '-sincwindow': value})
  }

  function onSincWidthChange(value){
    setOpts({...opts, '-sincwidth': value})
  }

  return (
    <AppContainer gap={2}>
      <Title>
        FLIRT
      </Title>
      {/* Input file chooser */}
      
      <FileChooser value={opts['-in']} textLabel="Input" onClick={onInputFileClick}/>
      <Row width={'100%'}>
      {/* DOF select */}
        <InputSelect options={dofOptions} value={12} label="DOF" width={'50%'} onChange={onDOFChange}/>
      </Row>

      {/* reference file chooser */}
      <FileChooser value={opts['-ref']} textLabel="Reference" onClick={onRefFileClick}/>
      <Row gap={2} width={'100%'} alignItems='center'>
        <Typography variant="body" fontFamily={'sans-serif'}>Choose reference:</Typography>
        <Button onClick={onMNI1mmClick}>MNI 1mm brain</Button>
        <Button onClick={onMNI2mmClick}>MNI 2mm brain</Button>
      </Row>
      {/* output file chooser */}
      <FileChooser value={opts['-out']} textLabel="Output"/>
      {/* command string */}
      <CommandPreview commandString={commandString} multiLine={true}/>
      {/* Row of buttons (run and options) */}
        <Row gap={2} marginTop={2}>
          {/* Run button */}
          <Button variant='contained' disabled={running} onClick={onRunClick}>Run</Button>
          {/* Options button */}
          <Button onClick={onOptionsClick}>Options</Button>
        </Row>
        {optionsOpen &&
          <Column gap={2} justifyContent={'left'} width={'100%'}>
            {/* Search select */}
            <InputSelect options={searchOptions} value={'-90 90'} label="Search" width={'50%'} onChange={onSearchChange}/>
            {/* cost function select */}
            <InputSelect options={costOptions} value={'corratio'} label="Cost function" width={'50%'} onChange={onCostChange}/>
            {/* number of histogram bins */}
            <InputSlider
              label="Number of histogram bins"
              value={256}
              max={1000}
              min={1}
              step={1}
              onChange={onHistBinsChange}
            />
            {/* interpolation method select */}
            <InputSelect options={interpOptions} value={'trilinear'} label="Interpolation" width={'50%'} onChange={onInterpChange}/>
            {/* sinc window options */}
            <InputSelect options={sincOptions} value={'hanning'} label="Sinc window" width={'50%'} onChange={onSincWindowChange}/>
            {/* sinc width */}
            <InputSlider
              label="Sinc width"
              value={7}
              max={20}
              min={1}
              step={1}
              onChange={onSincWidthChange}
            />
            {/* input weighting file */}
            <FileChooser value={opts['-inweight']} textLabel="Input weighting image" onClick={onInputWeightFileClick}/>

            {/* reference weighting file */}
            <FileChooser value={opts['-refweight']} textLabel="Reference weighting image" onClick={onRefWeightFileClick}/>
          </Column>
        }

    </AppContainer>
  )
}
