/**
 * object containing default flirt options for applyxfm
 * @memberof fsljs
 * @property {string} -in - The input file path.
 * @property {string} -ref - The reference file path.
 * @property {string} -out - The output file path.
 * @property {string} -omat - The output matrix file path.
 * @property {number} -dof - The degrees of freedom.
 */
export const defaultApplyXFMOpts = {
    '-in': '',
    '-applyxfm': true,
    '-init': '',
    '-ref': '',
    '-out': '',
    '-interp': 'trilinear',
    '-sincwidth': 7,
    '-sincwindow': 'hanning',
    '-paddingsize': 0.0
}