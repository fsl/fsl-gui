/**
 * object containing default fast options
 * @memberof fsljs
 */
export const defaultFastOpts = {
    '-n': 3,
    '-I': 4,
    '-l': 20,
    '-t': 1, // 1 = T1, 2 = T2, 3 = PD
    '-o': '', // output basename
    '-g': false,
    '-b': false,
    '-B': false,
    '-N': false,
    '--nopve': false,
    '-H': 0.1,
    '-I': 4,
    '-S': 1,
    'in': '', // input file path is positional so it should always be last (it can be multiple images separateed by a space)
}