
/**
 * @object defaultBetOpts
 * @memberof fsljs
 * @description The default options for the bet function.
 * @property {string} input - The input file path.
 * @property {string} output - The output file path.
 * @property {boolean} -o - Save brain surface outline.
 * @property {boolean} -m - Generate binary brain mask.
 * @property {boolean} -s - Generate approximate skull image.
 * @property {boolean} -n - Don't generate segmented brain image output.
 * @property {number} -f - Fractional intensity threshold (0->1); default=0.5; smaller values give larger brain outline estimates.
 * @property {number} -g - Vertical gradient in fractional intensity threshold (-1->1); default=0; positive values give larger brain outline at bottom, smaller at top.
 * @property {number} -r - Head radius (mm not voxels); initial surface sphere is set to half of this.
 * @property {string} -c - Centre-of-gravity (voxels not mm) of initial mesh surface.
 * @property {boolean} -t - Apply thresholding to segmented brain image and mask.
 * @property {boolean} -e - generates brain surface as mesh in .vtk format.
 * @property {boolean} -R - robust brain centre estimation (iterates BET several times).
 * @property {boolean} -S - eye & optic nerve cleanup (can be useful in SIENA).
 * @property {boolean} -B - bias field and neck cleanup.
 * @property {boolean} -Z - improve BET if FOV is very small in Z (by temporarily padding end slices).
 * @property {boolean} -F - apply to 4D FMRI data.
 * @property {boolean} -A - run bet2 and then betsurf and then bet2 again (to get additional skull and scalp surfaces).
 * @property {string} -A2 - as with -A, when also feeding in non-brain-extracted versions of the original images (requires BET 2.1).
 * @property {boolean} -v - Verbose mode.
 * @property {boolean} -d - Debug mode.
 */
export const defaultBetOpts = {
    'in':        '',    // input file path
    'out':       '',     // output file path
    '-o':           false,
    '-m':           false,
    '-s':           false,
    '-n':           false,
    '-f':           0.5,
    '-g':           0,
    '-r':           null,
    '-c':           null,
    '-t':           false,
    '-e':           false,
    '-R':           null,
    '-S':           null,
    '-B':           null,
    '-Z':           null,
    '-F':           null,
    '-A':           null,
    '-A2':          null,
    '-v':           false,
    '-d':           false
}
