/**
 * object containing default flirt options
 * @memberof fsljs
 * @property {string} -in - The input file path.
 * @property {string} -ref - The reference file path.
 * @property {string} -out - The output file path.
 * @property {string} -omat - The output matrix file path.
 * @property {number} -dof - The degrees of freedom.
 */
export const defaultFlirtOpts = {
    '-in': '',
    '-ref': '',
    '-out': '',
    '-omat': '',
    '-dof': 12,
    '-searchrx': '0 0',
    '-searchry': '0 0',
    '-searchrz': '0 0',
    '-cost': 'corratio',
    '-bins': 256,
    '-interp': 'trilinear',
    '-sincwidth': 7,
    '-sincwindow': 'hanning',
    '-refweight': '',
    '-inweight': '',
}