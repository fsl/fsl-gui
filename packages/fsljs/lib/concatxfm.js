/**
 * object containing default concatxfm options for convertxfm
 * @memberof fsljs
 */
export const defaultConcatXFMOpts = {
    '-omat': '',
    '-concat': true,
    'in': '',
}