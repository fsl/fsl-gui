import { useState, useEffect } from 'react'
import './App.css'
import {fsljs} from 'fsljs'
import { 
  AppContainer, 
  Title, 
  FileChooser, 
  Row, 
  Column, 
  CommandPreview, 
  NiiVue,
  InputSlider,
  InputSelect
} from 'widgets'
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';

export default function App() {

  const [optionsOpen, setOptionsOpen] = useState(false)
  const [running, setRunning] = useState(false)
  const [opts, setOpts] = useState(fsljs.defaultApplyXFMOpts)
  const [commandString, setCommandString] = useState('')


  // watch for changes to the JSON string version of opts
  useEffect(() => {
    // create the command string for this tool (sends a message to the main process)
    async function createCommandString(){
      let command = 'flirt'
      let commandString = await fsljs.createCommandString({command, opts})
      setCommandString(commandString)
    }
    autoSetOutputFile()
    createCommandString()
  }, [JSON.stringify(opts)])


  function onOptionsClick(){
    setOptionsOpen(!optionsOpen)
  }

  async function onRunClick(){
    setRunning(true)
    let output = await fsljs.run({
      command: 'flirt',
      opts: opts
    })
    console.log('from main', output)
    setRunning(false)
  }

  const interpOptions = [
      {value: 'trilinear', label: 'trilinear'},
      {value: 'nearestneighbour', label: 'nearestneighbour'},
      {value: 'sinc', label: 'sinc'},
      {value: 'spline', label: 'spline'},
  ]

  const sincOptions = [
      {value: 'rectangular', label: 'rectangular'},
      {value: 'hanning', label: 'hanning'},
      {value: 'blackman', label: 'blackman'},
  ]

  async function onInputFileClick(){
    let results = await fsljs.openFileDialog()
    console.log(results)
    let inputFile = results.filePaths[0]
    // update the '-in' option
    setOpts({...opts, '-in': inputFile})
  }

  async function onRefFileClick(){
    let results = await fsljs.openFileDialog()
    console.log(results)
    let inputFile = results.filePaths[0]
    // update the '-in' option
    setOpts({...opts, '-ref': inputFile})
  }

  async function onInitMatFileClick(){
    let results = await fsljs.openFileDialog()
    console.log(results)
    let inputFile = results.filePaths[0]
    // update the '-in' option
    setOpts({...opts, '-init': inputFile})
  }

  function onOutputFileChange(value){
    setOpts({...opts, '-out': value})
  }

  function onInterpChange(value){
    setOpts({...opts, '-interp': value})
  }

  function onSincWindowChange(value){
    setOpts({...opts, '-sincwindow': value})
  }

  function onSincWidthChange(value){
    setOpts({...opts, '-sincwidth': value})
  }

  function onPaddingChange(value){
    setOpts({...opts, '-paddingsize': value})
  }


  function autoSetOutputFile(){
    let inputFile = opts['-in']
    let refFile = opts['-ref']
    // do nothing if either inputFile or refFile are empty strings
    if (inputFile === '' || refFile === '' || inputFile === null || refFile === null) return
    let inputDirname = fsljs.dirname(inputFile)
    let inputBasename = fsljs.removeExtension(fsljs.basename(inputFile))
    let refBasename = fsljs.removeExtension(fsljs.basename(refFile))
    let outputFile = fsljs.join(inputDirname, `${inputBasename}_to_${refBasename}`)
    setOpts({...opts, '-out': outputFile})
  }


  return (
    <AppContainer gap={2}>
      <Title>
        Apply XFM
      </Title>
      <FileChooser value={opts['-init']} textLabel="Input transformation matrix (.mat file)" onClick={onInitMatFileClick}/>
      <FileChooser value={opts['-in']} textLabel="Input image" onClick={onInputFileClick}/>
      {/* reference image */}
      <FileChooser value={opts['-ref']} textLabel="Reference image" onClick={onRefFileClick}/>
      {/* output image */}
      <FileChooser value={opts['-out']} textLabel="Output image" onChange={onOutputFileChange}/>
      {/* command string */}
      <CommandPreview commandString={commandString} multiLine={true}/>
      {/* Row of buttons (run and options) */}
      <Row gap={2} marginTop={2}>
          {/* Run button */}
          <Button variant='contained' onClick={onRunClick} disabled={running}>Run</Button>
          {/* Options button */}
          <Button onClick={onOptionsClick}>Options</Button>
        </Row>
        {optionsOpen &&
          <Column gap={2} justifyContent={'left'} width={'100%'}>
            {/* interpolation method select */}
            <InputSelect options={interpOptions} value={opts['-interp']} label="Interpolation" width={'50%'} onChange={onInterpChange}/>
            {/* sinc window options */}
            <InputSelect options={sincOptions} value={opts['-sincwindow']} label="Sinc window" width={'50%'} onChange={onSincWindowChange}/>
            {/* sinc width */}
            <InputSlider
              label="Sinc width"
              value={opts['-sincwidth']}
              max={20}
              min={1}
              step={1}
              onChange={onSincWidthChange}
            />
            {/* extra voxel padding */}
            <InputSlider
              label="Extra voxel padding for output FOV"
              value={opts['-paddingsize']}
              max={100}
              min={0}
              step={1}
              onChange={onPaddingChange}
            />
          </Column>
        }

    </AppContainer>
  )
}
