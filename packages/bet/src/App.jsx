import { useState, useEffect, useRef } from 'react'
import './App.css'
import {fsljs} from 'fsljs'
import { 
  AppContainer, 
  Title, 
  FileChooser, 
  Row, 
  Column, 
  CommandPreview, 
  NiiVue,
  InputSlider
} from 'widgets'
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';

// the main app component for the bet tool that renders the UI
function App() {
  // set the initial state of versions to an empty string
  const [versions, setVersions] = useState('')
  // set the initial state of the command string to an empty string
  const [commandString, setCommandString] = useState('')
  // set the initial state of the opts object to the default options for this tool
  const [opts, setOpts] = useState(fsljs.defaultBetOpts)
  // set the initial state of the running flag to false. 
  // When running, the run button is disabled to prevent multiple commands from being run at the same time
  const [running, setRunning] = useState(false)
  // set the initial state of the nvImages array to an empty array
  const [nvImages, setNvImages] = useState([])
  // set the initial state of the commsInfo object to an empty object
  const [commsInfo, setCommsInfo] = useState({})
  // set the initial state of the optionsOpen flag to false
  const [optionsOpen, setOptionsOpen] = useState(false)

  // send a message to the main process to get the FSL version
  async function getVersions(){
    let versions = `FSL version ${await fsljs.fslVersion()}`
    console.log(versions)
    setVersions(versions)
  }


  // send a message to the main process to open a file dialog
  async function openFileDialog(){
    /*
    * results = {
    *   canceled: boolean,
    *  filePaths: string[]
    * }
    */
    let results = await fsljs.openFileDialog()
    return results 
  }

  // create the command string for this tool (sends a message to the main process)
  async function createCommandString(){
    let command = 'bet'
    let commandString = await fsljs.createCommandString({command, opts})
    setCommandString(commandString)
  }

  // loop over all keys in the opts object and use useEffect to watch for changes
  // only applies to keys that start with a dash
  Object.keys(opts).forEach((key) => {
    if (key.startsWith('-')){
      useEffect(() => {
        createCommandString()
      }, [opts[key]])
    }
  })

  // when the "in" property changes, set the "out" property to the same value + "_brain"
  useEffect(() => {
    let input = opts['in']
    if (input === ''){
      return
    }
    let output = fsljs.removeExtension(input) + '_brain.nii.gz'
    setOpts(opts => {
      return {...opts, ...{out: output}}
    })
    // setOpts({...opts, ...{out: output}})
    createCommandString()
    // set the nvImages array to the input and output paths
    let baseUrl = `http://${commsInfo.host}:${commsInfo.fileServerPort}/${commsInfo.route}?${commsInfo.queryKey}=`;
    let underlay = {
      url: `${baseUrl}${input}`,
      name: input,
      colormap: 'gray'
    }
    let overlay = {
      url: `${baseUrl}${output}`,
      name: output,
      colormap: 'red'
    }
    console.log(underlay, overlay)
    setNvImages([underlay])
  }, [opts['in']])

  // This is a special case because the output path is not set until the input path is set
  useEffect(() => {
    createCommandString()
  }, [opts['out']])


  // handler for the input text field change event
  function onInputChange(value){
    // set the input path
    setOpts({...opts, ...{['in']: value}})
  }

  // handler for the input button click event
  async function onInputClick(){
    // open the file dialog and get the file path
    const results = await openFileDialog()
    // set the input path
    let input = results.filePaths[0]
    setOpts({...opts, ...{in: input}})
  }

  // handler for the output text field change event
  function onOutputChange(value){
    // set the ouput path
    setOpts({...opts, ...{out: value}})
  }

  // handler for the output button click event
  function onOutputClick(){
    console.log('clicked')
  }

  // handler for the run button click event
  async function onRunClick(){
    setRunning(true)
    let command = 'bet'
    let runOpts = {
      command: command,
      opts: opts
    }
    let output = await fsljs.run(runOpts)
    console.log('from main', output)
    setRunning(false)
    let baseUrl = `http://${commsInfo.host}:${commsInfo.fileServerPort}/${commsInfo.route}?${commsInfo.queryKey}=`;
    let underlay = {
      url: `${baseUrl}${opts['in']}`,
      name: opts['in'],
      colormap: 'gray'
    }
    let overlay = {
      url: `${baseUrl}${opts['out']}`,
      name: opts['out'],
      colormap: 'red'
    }
    console.log(underlay, overlay)
    setNvImages([underlay, overlay])
  }

  function onOptionsClick(){
    setOptionsOpen(!optionsOpen)
  }

  // get the comms info from the main process
  // when the app is first loaded
  useEffect(() => {
    async function getCommsInfo(){
      let info = await fsljs.getCommsInfo()
      console.log(info)
      setCommsInfo(info)
    }
    getCommsInfo()
  }, [])

  function onLocationChange(location){
    // console.log(location)
    // get the vox property from the location object
    let vox = location.vox
    // create a string from the vox property
    let voxString = vox.join(' ')
    // set the opts -c flag to the vox property
    // setOpts({...opts, ...{'-c': voxString}})

  }


  return (
      <AppContainer gap={2}>
        <Title>Bet</Title>
        {/* Input select component */}
        <FileChooser value={opts['in']} textLabel="Input" onChange={onInputChange} onClick={onInputClick}/>
        {/* Output select component */}
        <FileChooser value={opts['out']} textLabel="Output" onChange={onOutputChange} onClick={onOutputClick}/>
        {/* command string */}
        <CommandPreview commandString={commandString} multiLine={true}/>
        {/* Row of buttons */}
        <Row gap={2} marginTop={2}>
          {/* Run button */}
          <Button onClick={onRunClick} variant='contained' disabled={running}>Run</Button>
          {/* Options button */}
          <Button onClick={onOptionsClick}>Options</Button>
        </Row>
        {/* options container */}
        {optionsOpen &&
          <Column gap={0} justifyContent={'left'} width={'100%'}>
            {/* fractional intensity */}
            <InputSlider
              label="Fractional intensity threshold"
              value={opts['-f']}
              max={1}
              min={0}
              step={0.01}
              onChange={(value) => setOpts({...opts, ...{'-f': value}})}
            />
            {/* gradient slider */}
            <InputSlider
              label="Threshold gradient. Positive values give larger brain outline at bottom"
              value={opts['-g']}
              max={1}
              min={-1}
              step={0.01}
              onChange={(value) => setOpts({...opts, ...{'-g': value}})}
            />
            {/* output brain image */}
            <FormControlLabel
              control={
                <Checkbox
                  checked={opts['-n']}
                  onChange={(e) => setOpts({...opts, ...{'-n': e.target.checked}})}
                />
              }
              label="Don't generate an output brain image"
            />
            {/* brain mask */}
            <FormControlLabel
              control={
                <Checkbox
                  checked={opts['-m']}
                  onChange={(e) => setOpts({...opts, ...{'-m': e.target.checked}})}
                />
              }
              label="Generate binary brain mask"
            />
            {/* skull image */}
            <FormControlLabel
              control={
                <Checkbox
                  checked={opts['-s']}
                  onChange={(e) => setOpts({...opts, ...{'-s': e.target.checked}})}
                />
              }
              label="Generate approximate skull image"
            />
            {/* overlay */}
            <FormControlLabel
              control={
                <Checkbox
                  checked={opts['-o']}
                  onChange={(e) => setOpts({...opts, ...{'-o': e.target.checked}})}
                />
              }
              label="Overlay onto original image"
            />
            {/* threshold */}
            <FormControlLabel
              control={
                <Checkbox
                  checked={opts['-t']}
                  onChange={(e) => setOpts({...opts, ...{'-t': e.target.checked}})}
                />
              }
              label="Apply thresholding to segmented brain image and mask"
            />
          </Column>
        }
        {/* niivue panel */}
        <NiiVue images={JSON.stringify(nvImages)} onLocationChange={onLocationChange}/>
      </AppContainer>
  )
}

export default App
