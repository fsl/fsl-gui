import { useState } from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

export function InputSelect({options, onChange=null, value, label, ...props}) {
  const [_value, setValue] = useState(value);

  const handleChange = (event) => {
    setValue(event.target.value);
    // check if onChange is defined
    if (onChange){
        onChange(event.target.value);
    }
  };

  return (
    <Box sx={{ 
        minWidth: 120, 
        width: props.width,
        display: 'flex',
        }}>
      <FormControl fullWidth>
        <InputLabel id="input-select-label">{label}</InputLabel>
        <Select
          labelId="input-select-label"
          id="input-simple-select"
          value={_value}
          label={label}
          onChange={handleChange}
          size='small'
        >
            {options.map((option) => (
                <MenuItem key={option.label} value={option.value}>{option.label}</MenuItem>
            ))}
        </Select>
      </FormControl>
    </Box>
  );
}