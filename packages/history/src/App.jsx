import { useState, useEffect} from 'react'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography'
import './App.css'
import {fsljs} from 'fsljs'
import { AppContainer, Title, CommandPreview} from 'widgets'

// the main app component for the bet tool that renders the UI
function App() {
  // set the initial state of the histort table to an empty array
  const [history, setHistory] = useState([])

  // when the component mounts, read the history from the main process
  // and set the state of the history table
  useEffect(() => {
    async function readHistory(){
      let h = await fsljs.readHistory()
      console.log(h)
      setHistory(h)
    }
    readHistory()
  }, [])


  return (
      <AppContainer gap={2}>
        <Title>History</Title>
        <TableContainer component={Paper}>
      <Table sx={{ width: '100%' }} aria-label="FSL GUI History">
        <TableHead>
          <TableRow>
            <TableCell>Command</TableCell>
            <TableCell align="left">Date</TableCell>
            <TableCell align="left">Full Command</TableCell>
            <TableCell align="left">StdOut</TableCell>
            <TableCell align="left">StdErr</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {history.map((row, index) => (
            <TableRow
              key={`${row.command}-${index}`}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                <Typography fontFamily={'monospace'}>{row.command}</Typography>
              </TableCell>
              <TableCell align="left">
                <Typography sx={{whiteSpace: 'nowrap'}} fontFamily={'monospace'}>{row.date}</Typography>
              </TableCell>
              <TableCell align="left">
                <CommandPreview commandString={row.fullCommand} />
              </TableCell>
              <TableCell align="left">
                <Typography sx={{whiteSpace: 'nowrap'}} fontFamily={'monospace'}>{row.stdout}</Typography>
              </TableCell>
              <TableCell align="left">
                <Typography sx={{whiteSpace: 'nowrap'}} fontFamily={'monospace'}>{row.stderr}</Typography>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
      </AppContainer>
  )
}

export default App
