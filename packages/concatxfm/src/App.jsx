import { useState, useEffect } from 'react'
import './App.css'
import {fsljs} from 'fsljs'
import { 
  AppContainer, 
  Title, 
  FileChooser, 
  Row, 
  Column, 
  CommandPreview, 
  NiiVue,
  InputSlider,
  InputSelect
} from 'widgets'
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';

export default function App() {

  const [running, setRunning] = useState(false)
  const [opts, setOpts] = useState(fsljs.defaultConcatXFMOpts)
  const [commandString, setCommandString] = useState('')
  const [aTob, setAToB] = useState('')
  const [bToC, setBToC] = useState('')

  // watch for changes to the JSON string version of opts
  useEffect(() => {
    // create the command string for this tool (sends a message to the main process)
    async function createCommandString(){
      let command = 'convert_xfm'
      let commandString = await fsljs.createCommandString({command, opts})
      setCommandString(commandString)
    }
    autoSetOutputFile()
    createCommandString()
  }, [JSON.stringify(opts)])

  // watch aTob and bToC for changes
  useEffect(() => {
    // update the opts
    setOpts({...opts, 'in': `${aTob} ${bToC}`})
  }, [aTob, bToC])

  async function onRunClick(){
    setRunning(true)
    let output = await fsljs.run({
      command: 'convert_xfm',
      opts: opts
    })
    console.log('from main', output)
    setRunning(false)
  }

  async function onInputFileABClick(){
    let results = await fsljs.openFileDialog()
    console.log(results)
    let inputFile = results.filePaths[0]
    // update aToB
    setAToB(inputFile)
  }

  async function onInputFileBCClick(){
    let results = await fsljs.openFileDialog()
    console.log(results)
    let inputFile = results.filePaths[0]
    // update bToC
    setBToC(inputFile)
  }

  function onOutputFileChange(value){
    setOpts({...opts, '-omat': value})
  }

  function autoSetOutputFile(){
    let inputaTob = aTob
    let inputbToc = bToC
    // do nothing if either inputFile or refFile are empty strings
    if (inputaTob === ''|| inputaTob === null || inputbToc === '' || inputbToc === null) return
    let inputDirname = fsljs.dirname(inputaTob)
    let inputABBasename = fsljs.removeExtension(fsljs.basename(inputaTob), '.mat')
    let inputBCBasename = fsljs.removeExtension(fsljs.basename(inputbToc), '.mat')
    let outputFile = fsljs.join(inputDirname, `concat_${inputABBasename}_to_${inputBCBasename}.mat`)
    setOpts({...opts, '-omat': outputFile})
  }
  
  return (
    <AppContainer gap={2}>
      <Title>
        Concat XFM
      </Title>
       {/* all of these file choosers take .mat files  */}
      <FileChooser value={aTob} textLabel="Transformation matrix for A to B" onClick={onInputFileABClick}/>
      <FileChooser value={bToC} textLabel="Transformation matrix for B to C" onClick={onInputFileBCClick}/>
      <FileChooser value={opts['-omat']} textLabel="Output (A to C)" onChange={onOutputFileChange}/>
      <CommandPreview commandString={commandString} multiLine={true}/>
      {/* Row of buttons (run and options) */}
        <Row gap={2} marginTop={2}>
          {/* Run button */}
          <Button variant='contained' onClick={onRunClick} disabled={running}>Run</Button>
        </Row>
    </AppContainer>
  )
}
