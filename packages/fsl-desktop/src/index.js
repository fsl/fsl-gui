const { app, BrowserWindow, Menu, ipcMain} = require('electron');
const path = require('path');
const { fork } = require("child_process");
const {devPorts} = require('./devPorts');
const {events} = require('./events');

// launch the fileServer as a background process
fileServer = fork(
  path.join(__dirname, "fileServer.js"),
  { env: { FORK: true } }
);

/**
 * handles setting the process env variables for the fileServer port and host
 * @param {number} port - the port the fileServer is listening on
 * @returns {undefined}
 * @function
 * @example
 * onFileServerPort(12345);
 * console.log(process.env.FSLGUI_FILESERVER_PORT);
 * // 12345
 * console.log(process.env.FSLGUI_HOST);
 * // localhost
 */
function onFileServerPort(port) {
  process.env.FSLGUI_FILESERVER_PORT = port;
  process.env.FSLGUI_HOST = 'localhost';
}

// handler function for the fileServer port message
function handleFileServerMessage(message) {
  // msg is expected to be a JSON object (automatically serialized and deserialized by process.send and 'message')
  // a message object has a 'type' and a 'value' as properties
  if (message.type === "fileServerPort") {
    onFileServerPort(message.value);
  }
}

// register the handler function for fileServer messages
fileServer.on("message", (message) => {
  handleFileServerMessage(message);
});


/**
 * Determines if the application is running in development mode.
 * @returns {boolean} True if the application is running in development mode, false otherwise.
 */
function isDev() {
  // process.argv is an array of the command line arguments
  // the first two are the path to the node executable and the path to the script being run
  // the third is the first argument passed to the app
  // if it's "--dev", we're in development mode
  return process.argv[2] === '--dev';
}

/**
 * Registers IPC listeners for the events object.
 */
function registerIpcListeners() {
  for (const [key, value] of Object.entries(events)) {
    /**
     * The handler function for the event.
     * @param {Electron.IpcMainInvokeEvent} event - The event object.
     * @param {object} obj - The object containing the event arguments.
     * @returns {Promise<any>} A promise that resolves to the result of the event handler.
     * @async
     * @function
     * @example
     * ipcRenderer.invoke('fslVersion').then((fslVersion) => {
     *  console.log(fslVersion);
     */
    async function handler(event, obj) {
      const result = await value(obj);
      return result;
    }
    ipcMain.handle(key, handler);
  }
}

/**
 * Launches an external GUI process.
 * @param {string} guiName - The name of the GUI to launch.
 */
function launchExternalGui(guiName) {
  const externalGuis = [
    'fsleyes'
  ];

  if (externalGuis.includes(guiName)) {
    const { spawn } = require('child_process');
    const child = spawn(guiName, [], {
      detached: true,
      stdio: 'ignore'
    });
    child.unref();
    return true;
  } else {
    return false;
  }
}

/**
 * Creates a new browser window for the specified GUI.
 * @param {string} guiName - The name of the GUI to create a window for.
 */
function createWindow(guiName) {

  if (launchExternalGui(guiName)) {
    return;
  } 
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 1000,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
    },
  });

  if (isDev()) {
    try {
      mainWindow.loadURL(`http://localhost:${devPorts[guiName]}`);
    } catch (err) {
      console.log(`Error loading ${guiName} at http://localhost:${devPorts[guiName]}`);
      console.log(err);
    }
    // Open the DevTools.
    mainWindow.webContents.openDevTools();
  } else {
    // get user home directory
    const homeDir = app.getPath('home');
    mainWindow.loadFile(path.join(homeDir, '.fslgui', guiName, 'index.html'));
  }
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', registerIpcListeners);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  // close the file server gracefully
  fileServer.kill()
  app.quit();
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    //createWindow();
  }
});

// create an application menu with an "apps" submenu that lists all the available FSL apps
// FSL apps are: bet, flirt, fast, fslstats
// each app should have a keyboard shortcut
let menu = [
  {
    label: 'Tools',
    submenu: [
      {
        label: 'bet',
        accelerator: 'CmdOrCtrl+B',
        click: () => {
          createWindow('bet');
        }
      },
      {
        label: 'flirt',
        accelerator: 'CmdOrCtrl+F',
        click: () => {
          createWindow('flirt');
        }
      },
      // applyxfm
      {
        label: 'applyxfm',
        click: () => {
          createWindow('applyxfm');
        }
      },
      // invertxfm
      {
        label: 'invertxfm',
        click: () => {
          createWindow('invertxfm');
        }
      },
      // concatxfm
      {
        label: 'concatxfm',
        click: () => {
          createWindow('concatxfm');
        }
      },
      {
        label: 'fast',
        accelerator: 'CmdOrCtrl+T',
        click: () => {
          createWindow('fast');
        }
      },
      {
        label: 'fslstats',
        accelerator: 'CmdOrCtrl+S',
        click: () => {
          createWindow('fslstats');
        }
      },
      {
        label: 'glm',
        accelerator: 'CmdOrCtrl+G',
        click: () => {
          createWindow('glm');
        }
      },
      {
        label: 'feat',
        accelerator: 'CmdOrCtrl+E',
        click: () => {
          createWindow('feat');
        }
      },
      {
        label: 'fsl prepare fieldmap',
        accelerator: 'CmdOrCtrl+P',
        click: () => {
          createWindow('fsl_prepare_fieldmap');
        }
      },
      {
        label: 'fslmaths',
        accelerator: 'CmdOrCtrl+M',
        click: () => {
          createWindow('fslmaths');
        }
      },
      {
        label: 'featquery',
        accelerator: 'CmdOrCtrl+Q',
        click: () => {
          createWindow('featquery');
        }
      },
      {
        label: 'melodic',
        accelerator: 'CmdOrCtrl+L',
        click: () => {
          createWindow('melodic');
        }
      },
      {
        label: 'fdt',
        accelerator: 'CmdOrCtrl+D',
        click: () => {
          createWindow('fdt');
        }
      },
      {
        label: 'fsleyes',
        accelerator: 'CmdOrCtrl+Y',
        click: () => {
          createWindow('fsleyes');
        }
      },
      // add a separator
      { type: 'separator' },
      // history gui
      {
        label: 'history',
        accelerator: 'CmdOrCtrl+O',
        click: () => {
          createWindow('history');
        }
      },

    ]
  },
  // add window menu with reload options
  {
    label: 'Window',
    submenu: [
      {
        label: 'Reload',
        accelerator: 'CmdOrCtrl+R',
        click: () => {
          BrowserWindow.getFocusedWindow().reload();
        }
      },
      {
        label: 'Toggle DevTools',
        accelerator: 'CmdOrCtrl+Shift+I',
        click: () => {
          BrowserWindow.getFocusedWindow().toggleDevTools();
        }
      }
    ]
  }
];
// Add macOS application menus
if (process.platform === 'darwin') {
  menu.unshift({
    label: app.name,
    submenu: [
      { role: 'about' },
      { type: 'separator' },
      { role: 'services', submenu: [] },
      { type: 'separator' },
      { role: 'hide' },
      { role: 'hideothers' },
      { role: 'unhide' },
      { type: 'separator' },
      { role: 'quit' }
    ]
  });
}

Menu.setApplicationMenu(Menu.buildFromTemplate(menu));

